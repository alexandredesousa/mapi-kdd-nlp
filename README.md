# mapi-kdd-nlp

Project for the MAP-i's Knowledge Discovery from Databases course work on Natural Language Processing.

# Code
The code is in the notebook [mapi-kdd-nlp](mapi-kdd-nlp.ipynb) in the root of the project.

# Text file sources
The .txt files used as source, in the [data dir](data) were obtained in the [Project Gutenberg](https://www.gutenberg.org).